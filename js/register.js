//  1. when we click on the submit button then the form should be validate.
//  2. if there are any errrors the form should show the error messages
//  3. If there are no errors then the form should be submitted

//  Implementation
//  1. We have to create a reference to the button inside a variable 
//  2. Then we should define function which makes the validation  
//  3. we should set the 'onclick' eventhandler property to the varialble and it should be equal to the function.   


//  This makes the DOM load completely.
window.onload = function () {

    var registerButton = document.getElementById("register-button");
    if (registerButton != null) {
        registerButton.onclick = validateRegistratrionForm;
    }
}

//  The regular expression is used to validate an e-mail address.
var emailPattern = /^[a-zA-Z]+([_\.-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9]+([\.-]?[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,4})+$/;


function validateRegistratrionForm() {

    //  Contains the alert messages displayed to the user.
    var alertTxt = "";

    //  Get a reference to the email address field to read its input.
    var email = document.getElementById("email");

    //  Check if the element is valid before using it to avoid errors.
    if (email) {
        // Read the user input and stores in the loacal variable
        var emailVal = email.value;

        //  Check if there is any input.
        if (emailVal.length == 0) {
            alertTxt = alertTxt + "The E-mail address is required.\n";
        } else {
            //  There is some input.Check if it is a valid e-mail address.
            if (emailVal.match(emailPattern)) {
                // Valid e-mail address.continue.
            } else {
                alertTxt = alertTxt + "The E-mail address must be in a valid format . \n";
            }

        }
    }

    // Get reference to the password field to read its value.
    var password = document.getElementById("password");

    // Check if the element is valid before using it to avoid errors.
    if (password) {
        var passwordVal = password.value;

        // Check if there is any input.
        if (passwordVal.length == 0) {
            alertTxt = alertTxt + "The Password is required. \n";
        } else {

            // Check for the minimum length.
            if (passwordVal.length < 8) {
                alertTxt = alertTxt + "The Password must be at least 8 characters long. \n";
            }
        }
    }

    // Get reference to the Confirm password fiels to read its value.
    var confirmPassword = document.getElementById("confirm-password");

    // Check if the element is valid before using it to avoid errors.
    if (confirmPassword) {

        // Read the user input and store in a local varaible.
        var confirmPasswordVal = confirmPassword.value;

        // Check if there is any input.
        if (confirmPasswordVal.length == 0) {
            alertTxt = alertTxt + "The Confirm Password is required.\n";
        } else {

            // Check if the Confirm password and Password are equal.
            if (confirmPasswordVal != passwordVal) {
                alertTxt = alertTxt + "Please make sure that the password and confirm password must be same.\n";
            }
        }
    }

    // Get reference to the First name fiels to read its value.
    var firstName = document.getElementById("first-name");

    // Check if the element is valid before using it to avoid errors.
    if (firstName) {

        // Read the user input and store in a local varaible.
        var firstNameVal = firstName.value;

        // check if there is any input.
        if (firstNameVal == 0) {
            alertTxt = alertTxt + "The First Name is required.\n";
        }
    }

    // Get reference to the Last name fiels to read its value.
    var lastName = document.getElementById("last-name");

    // Check if the element is valid before using it to avoid errors.
    if (lastName) {

        // Read the user input and store in a local varaible.
        var lastNameVal = lastName.value;

        //  Check if there is any input.
        if (lastNameVal == 0) {
            alertTxt = alertTxt + "The Last Name is required.\n";
        }
    }

    // Check if there are any errors.

    if (alertTxt.length != 0) {

        // If yes display error messages.
        alert(alertTxt);

        // Return false so that form does not submit.
        return false;
    } else {

        // No errors,return true so that the form can submit.
        return true;
    }
}
