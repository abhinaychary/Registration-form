$(document).ready(function () {

    // We are going to run form valdiation on #registration-form element. 
    $("#registration-form").validate({
        // Specifying the validation rules.
        rules: {
            // Email is required and must be in the form of a valid email address.
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8
            },
            // Check if confirm-password is equalTo password.
            "confirm-password": {
                required: true,
                equalTo: "#password"
            },
            "first-name": {
                required: true
            },
            "last-name": {
                required: true
            }
        },
        // Specifying the validation error messages.
        messages: {
            email: {
                required: "The E-mail address is required.",
                email: "Enter a valid E-mail address."
            },
            password: {
                required: "The Password is required.",
                minlength: "Password must have at least 8 characters."
            },
            "confirm-password": {
                required: "The Confirm password is required.",
                equalTo: "Please Confirm password."
            },
            "first-name": {
                required: "The First name is required."
            },
            "last-name": {
                required: "The Last name is required."
            }
        },
        // Calls for Handling the submit when the form is valid.
        submitHandler: function (form) {
            form.submit();
        }
    });
});